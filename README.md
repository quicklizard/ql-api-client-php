# Quicklizard API Client - PHP

Authenticate and perform operations against Quicklizard's REST API from your PHP application.

## Installation

Add this line to your application's composer.json:

```json
{
  "require": {
    "quicklizard/api-client": "master"
  },
  "repositories": [
    {
      "type": "vcs",
      "url":  "git@bitbucket.org:quicklizard/ql-api-client-php.git"
    }
  ]
}
```

And then execute:

    $ composer install

## Usage

```php
//replace API_KEY and API_SECRET with your API credentials
//replace CLIENT_KEY with your client key
$client = new QL\API\Client('https://rest.quicklizard.com', 'API_KEY', 'API_SECRET');
$response = $client->get('/api/v2/recommendations/all?client_key=CLIENT_KEY');
print_r(json_decode($response));
```

### GET Requests

```php
//replace API_KEY and API_SECRET with your API credentials
//replace CLIENT_KEY with your client key
$client = new QL\API\Client('https://rest.quicklizard.com', 'API_KEY', 'API_SECRET');
$response = $client->get('/api/v2/recommendations/all?client_key=CLIENT_KEY');
print_r(json_decode($response));
```
---
### POST Requests

```php
//replace API_KEY and API_SECRET with your API credentials
//replace API_KEY and API_SECRET with your API credentials
//replace CLIENT_KEY with your client key
$client = new QL\API\Client('https://rest.quicklizard.com', 'API_KEY', 'API_SECRET');
$payload = array(
  "payload" => array(
    array(
      "client_key" => "CLIENT_KEY" .... //see API docs for complete structure
    )
  )
);
$response = $client->post('/api/v2/products/create'. $payload);
print_r(json_decode($response));
```
---
### PUT Requests

```php
//replace API_KEY and API_SECRET with your API credentials
//replace API_KEY and API_SECRET with your API credentials
//replace CLIENT_KEY with your client key
$client = new QL\API\Client('https://rest.quicklizard.com', 'API_KEY', 'API_SECRET');
$payload = array(
  "payload" => array(
    array(
      "client_key" => "CLIENT_KEY" .... //see API docs for complete structure
    )
  )
);
$response = $client->put('/api/v2/products/update'. $payload);
print_r(json_decode($response));
```
---

## Contributing

Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.org/quicklizard/ql-api-client-php.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

