<?php

namespace QL\API;

class Client
{
  
  /** @var null|string */
  private $api_key = null;
  
  /** @var null|string */
  private $api_secret = null;
  
  /** @var string */
  private $host = null;
  
  /**
   * Create a new Client Instance
   *
   * @param string $host
   * @param string $api_key
   * @param string $api_secret
   */
  public function __construct($host, $api_key, $api_secret)
  {
    $this->configure($host, $api_key, $api_secret);
  }
  
  /**
   * Configure client api key and secret
   *
   * @param string $host
   * @param string $api_key
   * @param string $api_secret
   */
  public function configure($host, $api_key, $api_secret){
    $this->host = $host;
    $this->api_key = $api_key;
    $this->api_secret = $api_secret;
  }
  
  /**
   * Perform HTTP GET request to API endpoint
   * @param string $endpoint API request endpoint. Example: '/api/v2/products?page=1'
   * @return string response
   */
  public function get($endpoint) {
    $url = $this->getURI($endpoint);
    $endpoint = $this->getAPIEndpoint($url['path'], $url['query']);
    $digest = $this->signRequest($url['path'], $url['query']);
    return $this->doRequest($endpoint, 'GET', null, $digest);
  }
  
  /**
   * Perform HTTP POST request to API endpoint with payload
   * @param string $endpoint API request endpoint. Example: '/api/v2/products/bulk_create'
   * @param json $payload API request body
   * @return string response
   */
  public function post($endpoint, $payload) {
    $url = $this->getURI($endpoint);
    $endpoint = $this->getAPIEndpoint($url['path'], $url['query']);
    $digest = $this->signRequest($url['path'], $url['query'], $payload);
    return $this->doRequest($endpoint, 'POST', $payload, $digest);
  }
  
  /**
   * Perform HTTP PUT request to API endpoint with payload
   * @param string $endpoint API request endpoint. Example: '/api/v2/products/bulk_update'
   * @param json $payload API request body
   * @return string response
   */
  public function put($endpoint, $payload) {
    $url = $this->getURI($endpoint);
    $endpoint = $this->getAPIEndpoint($url['path'], $url['query']);
    $digest = $this->signRequest($url['path'], $url['query'], $payload);
    return $this->doRequest($endpoint, 'PUT', $payload, $digest);
  }
  
  /**
   * Perform a file upload request to API endpoint
   * @param string $endpoint API request endpoint. Example: '/api/v2/products/bulk_update'
   * @param string $local_file path to local file to upload
   * @param string $field_name name of POST body field that contains the file contents. defaults to 'upload'
   * @return string response
   */
  public function upload($endpoint, $local_file, $field_name = 'upload') {
    $url = $this->getURI($endpoint);
    $endpoint = $this->getAPIEndpoint($url['path'], $url['query']);
    $digest = $this->signRequest($url['path'], $url['query']);
    $fileUpload = $this->makeCurlFile($local_file);
    $payload = array($field_name => $fileUpload);
    return $this->doRequest($endpoint, 'POST', $payload, $digest, true);
  }
  
  /**
   * Perform actual HTTP request with cUrl
   * @param string $endpoint API request endpoint (path + query-string)
   * @param string $method HTTP request method
   * @param json | null $payload API request body
   * @param string $digest API request signature
   * @param boolean $is_upload indicate whether this is a file upload request
   * @return string response
   */
  private function doRequest($endpoint, $method, $payload = null, $digest, $is_upload = false) {
    $headers = array(
      'API_KEY: '.$this->api_key,
      'API_DIGEST: '.$digest,
    );
    $curl_options = array(
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_URL => $this->host . $endpoint,
      CURLOPT_FOLLOWLOCATION => true,
    );
    if ($payload !== null) {
      if ($is_upload) {
        $_payload = $payload;
        $curl_options[CURLOPT_SAFE_UPLOAD] = 1;
      } else {
        $_payload = json_encode($payload);
        array_push($headers, 'Content-Type: application/json');
        array_push($headers, 'Content-Length: ' . strlen($_payload));
      }
    } else {
      $_payload = null;
    }
    $curl_options[CURLOPT_HTTPHEADER] = $headers;
    if ($method === 'POST') {
      $curl_options[CURLOPT_POST] = true;
      $curl_options[CURLOPT_POSTFIELDS] = $_payload;
    } else if ($method === 'PUT') {
      $curl_options[CURLOPT_CUSTOMREQUEST] = 'PUT';
      $curl_options[CURLOPT_POSTFIELDS] = $_payload;
    }
    $curl = curl_init();
    curl_setopt_array($curl, $curl_options);
    $result = curl_exec($curl);
    curl_close($curl);
    return $result;
  }
  
  /**
   * Join path and query string and return API request endpoint
   * @param string $path API request path
   * @param string $qs API request query-string
   * @return string
   */
  private function getAPIEndpoint($path, $qs) {
    return $path . '?' . $qs;
  }
  
  /**
   * Adds timestamp query-string parameter to endpoint and returns a parsed URL object from API host & endpoint
   * @param string $endpoint API request endpoint
   * @return parsed URL
   */
  private function getURI($endpoint) {
    $now = time() * 1000;
    if (strpos($endpoint, '?') !== false) {
      $path = $endpoint . '&qts=' . $now;
    } else {
      $path = $endpoint . '?qts=' . $now;
    }
    $fullURL =  $this->host . $path;
    return parse_url($fullURL);
  }
  
  /**
   * Takes request parts and returns a SHA256 hash of those parts that is used as signature during authentication
   * @param string $path API request path
   * @param string|null $qs API request query string
   * @param json|null $body API request body
   * @return string API request signature
   */
  private function signRequest($path, $qs = null, $body = null) {
    $parts = array($path);
    if ($qs !== null) {
      array_push($parts, $qs);
    }
    if ($body !== null) {
      array_push($parts, json_encode($body));
    }
    array_push($parts, $this->api_secret);
    $digest = hash('sha256',implode('',$parts));
    return $digest;
  }
  
  private function makeCurlFile($file){
    $mime = mime_content_type($file);
    $info = pathinfo($file);
    $name = $info['basename'];
    $output = new \CURLFile($file, $mime, $name);
    return $output;
  }
}