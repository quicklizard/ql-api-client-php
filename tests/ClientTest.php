<?php

namespace QL\API;

class ClientTest extends \PHPUnit_Framework_TestCase
{

  private $client;

  protected function setUp()
  {
    $this->client = new Client('', '', '');
  }

  /**
   * Test GET request
   */
  public function testGet()
  {
    $response = $this->client->get('/api/v2/products?page=1&per_page=1');
    $data = json_decode($response);
    $results = count($data->{"result"});
    $this->assertTrue($results === 1);
  }

  /**
   * Test POST request
   */
  public function testPost()
  {
    $payload = array(
      "payload" => array(
        array(
          "client_uid" => "123"
        )
      )
    );
    $response = $this->client->post('/api/v2/products/bulk_create', $payload);
    $data = json_decode($response);
    $message = $data->{"message"};
    $this->assertTrue($message === 'Created 0 products');
  }

  /**
   * Test PUT request
   */
  public function testPut()
  {
    $payload = array(
      "payload" => array(
        array(
          "client_uid" => "123"
        )
      )
    );
    $response = $this->client->put('/api/v2/products/bulk_update', $payload);
    $data = json_decode($response);
    $message = $data->{"message"};
    $this->assertTrue($message === 'Updated 0 products');
  }

  /**
   * Test upload request
   */
  public function testUpload()
  {
    $response = $this->client->upload('/api/v2/products/upload?client_key=test_client', '/Volumes/Users/zohararad/Downloads/upload_template.txt');
    $data = json_decode($response);
    $message = $data->{"message"};
    print_r(strpos($message, 'file uploaded as test_client'));
    $this->assertEquals(0, strpos($message, 'file uploaded as test_client'));
  }
}